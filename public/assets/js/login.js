console.log("ad")

let loginForm = document.querySelector('#loginUser');

loginForm.addEventListener("submit", (e)=>{
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let  password = document.querySelector("#password").value
	console.log(email)
	console.log(password)

	// how can we inform a user that a blank input field cannot be logged in?
	if (email == "" || password == "") {
		// alert("Please input your email and/or password")
		 Swal.fire({
                            icon: 'error',
                            title: 'Ooppss!!', //the value of the title will be up the dev
                            text: 'Please input all required items.'//can be used to display/show more details/info about the action/response
                          });
	} else {
		fetch('https://stark-temple-78589.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res=>{
			return res.json()
		}).then(data =>{
			console.log(data.access)
			if (data.access) {
				//lets save the access token inside our local storage
				localStorage.setItem('token', data.access)
				//this local storage can be found inside the sibfolder of the appData of the local file of the google chrom browser inside the data module of the user folder
				// alert("access key saved on localStorage")
				 Swal.fire({
                            icon: 'success',
                            title: 'Welcome!', //the value of the title will be up the dev
                            text: 'Enjoy our courses'//can be used to display/show more details/info about the action/response
                          });
				fetch(`https://stark-temple-78589.herokuapp.com/api/users/details`, {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res=>{
					return res.json()
				}).then(data=> {
					console.log(data)
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					console.log("items are set inside the local storage")
					window.location.replace('./courses.html')
				})
			} else {
				//if there is no existing access key value from the data variable then just inform the user
				// alert("Something went wrong. Check your credentials")
				Swal.fire({
                            icon: 'warning',
                            title: 'Welcome back!', //the value of the title will be up the dev
                            text: 'Something went wrong. Check your credentials'//can be used to display/show more details/info about the action/response
                          	 
                          	 
                          });
			}
		})
	}

})