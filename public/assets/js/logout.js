// clear /wipeout all the data inside our local storage so that the session of the user will end
localStorage.clear()
//the clear method will allow you to remove the content of the storage object

//redirect the user to the login page just incase a new user wants to login
// window.location.replace('./login.html')