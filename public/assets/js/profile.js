console.log("sd")

// get the access token in the localStorage
let token = localStorage.getItem("token")
console.log(token)


let profile = document.querySelector("#profileContainer")

//lets create a control structure that will determine the display if the access token is null or empty

if (!token || token === null) {
	// lets refirect the user to the login page
	alert("You must login first");
	window.location.href="./login.html"
} else {
	// console.log("Token") 
	fetch(`https://stark-temple-78589.herokuapp.com/api/users/details`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data => {
		console.log(data);
	let enrollmentData = data.enrollments.map(classData => {
			console.log(classData)
			return (
				`
					<tr>
						<td>${classData.courseId}</td>
						<td>${classData.enrolledOn}</td>
						<td>${classData.status}</td>
					</tr>
				`
				)
		}).join("")
		profile.innerHTML = `

		<div class="col-md-12">
			<section class="jumbotron my-5">
				<h3 class="text-center">First Name: ${data.firstName}</h3>
				<h3 class="text-center">Last Name: ${data.lastName}</h3>
				<h3 class="text-center">Email: ${data.email}</h3>
				<table class="table">
					<thead>
						<th>Course ID:</th>
						<th>Enrolled On:</th>
						<th>Status:</th>
						<tbody>
						
						</tbody>${enrollmentData}</tbody>
					</thead>
				</table>
			</section>
		</div>



		`
	})
}