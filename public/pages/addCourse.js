

// Insert a new course
let createCourse = document.querySelector("#createCourse");

createCourse.addEventListener("submit", (event) =>{

   event.preventDefault();
   // lets capture values inside the input field
   let courseName = document.querySelector("#courseName").value
           // .value => describes the value attribute of the HTML element
   let coursePrice = document.querySelector("#coursePrice").value
           // 
   let courseDescription = document.querySelector("#courseDescription").value
           // 


// information validation upon creating a new entry in the database. Lets create a new structure


/**/

// for( let i = 0; i < name.length; i++) { Letss declare a variable that will describe the start index
// let courseName = name.toLowerCase()}

if((courseName !== "" && coursePrice !== null && courseDescription !== "" )){
 //  for( courseName = 0; courseName < courseName.length; courseName++) { 
 // courseName.toLowerCase()}

                  fetch("https://stark-temple-78589.herokuapp.com/api/courses/courseExists",{
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      name: courseName
                    })
                  }).then(res=>res.json()
                        )//this will gibe the info if there are no duplicates found
                    .then(data => {
                        if (data === false){
            /**/

/**/
    //save the new entry inside the database ny describing the request method/structure

       
      fetch("https://stark-temple-78589.herokuapp.com/api/courses/addCourse", {
        // here we built the structure of our request
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        } ,
        body: JSON.stringify({
            // once u have identified the properties of the document
            name: courseName,
            description: courseDescription,
            price: coursePrice

        })
    
        // next is to create a promise to identify what will happen if the response from the backend is posivite or negative

    }).then(res =>{
        return res.json() //the response I got is either true or false
        // then lets create another promise that will display the response from the backend to our front end
    }).then(data => {
        console.log(data) // check if we were able to get the data from our backend
        // create a control structure that will determine the response in my front end app
        if (data === true) {
            // alert("Course successfully added")
             Swal.fire({
                            icon: 'success',
                            title: 'Congrats!', //the value of the title will be up the dev
                            text: 'Course successfully added'//can be used to display/show more details/info about the action/response
                          });
        }else {
            alert("Something went wrong")
        }
    })//after describing the structure of the request body, now create the structure of the response coming from the backend
      } else {
                        // alert("Course already exist")
                          Swal.fire({
                            icon: 'error',
                            title: 'Ooppss!!', //the value of the title will be up the dev
                            text: 'Course already exists'//can be used to display/show more details/info about the action/response
                          });
                        
                        }

                    })


            }else {
                // alert("Something went wrong please check your credentials")
                Swal.fire({
                            icon: 'warning',
                            title: 'Oppss!', //the value of the title will be up the dev
                            text: 'Something went wrong please check your credentials'//can be used to display/show more details/info about the action/response
                          });
           }
         })