console.log("hello")

// the first thing that we need to do is to identify which course it needs to display inside the browser.
//we are going to use the course id to identify the correct course properly
let params = new URLSearchParams(window.location.search)
// window.location -> returns a location obj with ifo about the "current" loc of the doc
// search -> contains the query string section of the  current URL
// QUERY STRING IS AN OBJECT // search proprty return an object type stringStrng
// URLSearchParams() -> this method/constructor create and return a URLSearchParams object
// "URLSearchParams" -> this describes the interface that defines utility methods to work with the query string of a URL (this is the prop type)
// new -> instantiate a user-defined object
// yes it is a class -> template for creating the object
	// params = {
	// 	"courseId": ".... id ng course that we passed"
	// }

let id = params.get('courseId')
console.log(id)

// lets capture the access token from the local storage
	let token =	localStorage.getItem('token')
	console.log(token) // 
// lets capture the section of the html body
 let name = document.querySelector("#courseName");
  let desc = document.querySelector("#courseDesc");
   let price = document.querySelector("#couesePrice");
let enroll = document.querySelector("#enrollmentContainer");
   fetch(`https://stark-temple-78589.herokuapp.com/api/courses/${id}`).then(res=> res.json()).then(data=>{
   	console.log(data)



/**/

name.innerHTML = data.name;
      desc.innerHTML = data.description;
      price.innerHTML = data.price;
      enroll.innerHTML = `    
      <a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>
      `;


// addEventListener for enroll button
     document.querySelector("#enrollButton").addEventListener("click", () =>{
     		fetch(`https://stark-temple-78589.herokuapp.com/api/users/enroll`, {
     			//describe the parts of the request
     			method: 'POST',
     			headers: {
     				"Content-Type": "application/json",
     				"Authorization": `Bearer ${token}`
     			},
     			body: JSON.stringify({
     				courseId: id
     			})
     		}).then(res => {
          return res.json();
        }).then(data => {
          console.log(data);
          if(data === true) {
            Swal.fire({icon: "success",
            title: "Success",
            text: "successfully enrolled",
            confirmButtonText: "Back"
               }).then(()=> {
                  window.location.href = "./courses.html"
               })
          }else {
          	//inform the user that the req has failed
          	alert("Something went wrong")
          }
     })
  
})

	})