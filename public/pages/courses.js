console.log('sd')

let modalButton = document.querySelector('#adminButton')
// CAPTURE the html bofu which will display the content coming from the db

let container = document.querySelector('#coursesContainer')
let cardFooter;
let isAdmin = localStorage.getItem("isAdmin")

if (isAdmin == "false" || !isAdmin) {
	// id a user is a regular user, do not show the add course button
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = `
	  <div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-login">Add Course</a>
      </div>
	`
}
/*NEW*/

	// if ((container !== "" && container !== "") && (container === container)){

 //                  fetch("http://localhost:4000/api/users/email-exists",{
 //                    method: 'POST',
 //                    headers: {
 //                      'Content-Type': 'application/json'
 //                    },
 //                    body: JSON.stringify({
 //                      email: email
 //                    })
 //                  }).then(res=>res.json()
 //                        )//this will gibe the info if there are no duplicates found
 //                    .then(data => {
 //                        if (data === false){
/*ADDED*/

fetch('https://stark-temple-78589.herokuapp.com/api/courses/').then(res=> res.json()).then(data=>{
	console.log(data);
	// declare a variable that will display a result in the browser depending on the return
	let courseData;
	// create a control structure that will determin the value that the variable will hold
	if (data.length < 1) {
		courseData = "No Course Available"
	}else {
		// we will iterate the courses collection and display each course inside the browser
	courseData =	data.map(course => {
			// lets check the make up of each element inside the courses collection
			console.log(course._id);

			//if the user is a regular user, display the enroll button and display course button.
			if (isAdmin == "false" || !isAdmin) {
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-view-course text-white btn-block">View course details</a>

				
				`
				// <a href="#" class="btn btn-success text-white btn-block">Enroll</a>
			} else {
				cardFooter = `
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-view-course text-white btn-block">Edit</a>		
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-view-course text-white btn-block">Disable Course</a>
				`
			}
			return(
				`	<div class="col-md-6 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${course.name}</h5>
						<p class="card-text text-left">${course.description}</p>
						<p class="card-text text-left">${course.price}</p>
						<p class="card-text text-left">${course.createdOn}</p>
					</div>
					<div class="card-footer">
						${cardFooter}
					</div>
				</div>
			</div>
`			// we attched a query string in the irl which allows us to emed the ID from teh database record into 				the query string
		//  ? -> insidde the url "acts" as a "seperator" it indicated the end of the URL resurce path and indicates the start of the *query string*
		// # -> this waas originally used to jump to an specific element with the same id name or value
				)
		}).join("")// we used the join() method to create a return of new string
			// it concatenated all the objects inside the array and conberted each to a tring data type
	}
	container.innerHTML = courseData;
})

