

// Insert a new User
let registerUserForm = document.querySelector("#registerUser");

registerUserForm.addEventListener("submit", (event) =>{

   event.preventDefault();
   // lets capture values inside the input field
   let firstName = document.querySelector("#firstName").value
           // console.log(fName)
   let lastName = document.querySelector("#lastName").value
           // console.log(lName)
   let email = document.querySelector("#userEmail").value
           // console.log(email)
   let mobileNo = document.querySelector("#mobileNumber").value
           // console.log(mobileNo)
   let password1 = document.querySelector("#password1").value
       // console.log(password)
   let verifyPassword = document.querySelector("#password2").value
         // console.log(verifypassword)

// information validation upon crating a new entry in the database. Lets create a new structure

         if ((password1 !== "" && verifyPassword !== "") && (verifyPassword === password1) &&( mobileNo.length === 11)){

                  fetch("https://polar-lowlands-79076.herokuapp.com/api/users/emailExists",{
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      email: email
                    })
                  }).then(res=>res.json()
                        )//this will gibe the info if there are no duplicates found
                    .then(data => {
                        if (data === false){
            /**/
            fetch("https://polar-lowlands-79076.herokuapp.com/api/users/register", {
                    // here we built the structure of our request
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    } ,
                    body: JSON.stringify({
                        // once u have identified the properties of the document
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1,

                    })
                
                    // next is to create a promise to identify what will happen if the response from the backend is posivite or negative

                }).then(res =>{
                    return res.json() //the response I got is either true or false
                    // then lets create another promise that will display the response from the backend to our front end
                }).then(data => {
                    console.log(data) // check if we were able to get the data from our backend
                    // create a control structure that will determine the response in my front end app
                    if (data === true) {
                        // alert("New user registered successfully")
                         Swal.fire({
                            icon: 'success',
                            title: 'Congrats!', //the value of the title will be up the dev
                            text: 'New user registered successfully'//can be used to display/show more details/info about the action/response
                          });
                        window.location.replace('./courses.html');//new
                    }else {
                        // alert("Something went wrong")
                         Swal.fire({
                            icon: 'info',
                            title: 'Sorry', //the value of the title will be up the dev
                            text: 'Something went wrong. Please check credentials'//can be used to display/show more details/info about the action/response
                          });
                    }
                })
            /**/
                 } else {
                        // alert("Email already exists")
                         Swal.fire({
                            icon: 'error',
                            title: 'Ooppss!!', //the value of the title will be up the dev
                            text: 'Email already exists'//can be used to display/show more details/info about the action/response
                          });
                        }

                    })


            }else {
                // alert("Something went wrong please check your credentials")
                Swal.fire({
                  icon: 'error',
                  title: 'Ooppss!!', //the value of the title will be up the dev
                  text: 'Please input all required items.'//can be used to display/show more details/info about the action/response
                });
           }
         })